# Software

- `quanser_original` is the code that can be obtained on the dedicated page on [the product web](https://www.quanser.com/products/active-suspension/) page on Quanser website. Just for archiving purposes here, possibly for use with some older (pre R2021a) release of Matlab and Simulink. **Do not use!**

- `quanser_updated` is the Quanser code after some modifications and upgrades useful or even necessary for our setup. In particular, the data acquisition board was set correctly to our `QPID` DAQ, the configuration file `quanser_win64.tlc` for 64-bit Windows was correctly set, and the Simulink files were saved in the new SLX format (corresponding to R2021a).