# Quanser Active Suspension

This is a repository with supporting material (texts, data, codes, ...) for [Quanser Active Suspension](https://www.quanser.com/products/active-suspension/) laboratory experiment.

![Quanser Active Suspension](figures/active_suspension_photo_with_details.png)

See the video demonstration by clicking on the screenshot below – the subsequent discussion will be easier to digest.

[![Quanser Active Suspension demo](https://img.youtube.com/vi/jQXaMMotblE/0.jpg)](https://youtu.be/jQXaMMotblE)


## Control Systems Challenge

See the video and the documentation.

## Instructions

1. Make sure that the the RED BUTTON for stopping the platform by deactivating the amplifiers will be within your reach during the experiments. You will press it if anything goes wrong with the platform to protect it.

3. Switch on the [AMPAQ-L2 Amplifier](https://www.quanser.com/products/ampaq-l2-amplifier/) – the switch is on the back side just above the power cable. 

4. Move the lowest (road-level) stage manually upwards and locate the upper limit (the red LED on the back of the system turns on), move it downwards and locate the lower limit (the red LED on the back of the system turns on), finally position it roughly in between the limit positions and press the limit button on the back side of the experiment (the green LED turns on).

5. Get the directory [software/quanser_updated](software/quanser_updated) with the code for running the experiment. 

6. Alternatively you could get the code by clicking on the [Simulink Courseware](https://quanserinc.box.com/shared/static/65tv2urk3h0w4a7maoh550e9ucmamuj2.zip) link at the [Active Suspension ](https://www.quanser.com/products/active-suspension/) web page. It will download a zip archive. After unzipping there are two subdirectories: `Software` and `Documentation`. But we discourage you from doing this because you would then have to make a few changes/updates to their code (see [TROUBLESHOOTING.md](TROUBLESHOOTING.md)). We put this information here just to indicate where the code is originally coming from. 

7. Launch Matlab (R2021a) and go to the [software/quanser_updated](software/quanser_updated) subdirectory. There is a single m-file, it is named `setup_as.m`, which reveals its purpose – it sets all the parameters needed later. Run it in Matlab. 

8. Open any of the two simulink models whose names start with `q_`. These are essentially "Hello World!" basic examples for this experiment. One is open-loop, the other contains a controller. The remaining Simulink model starting with `s_` is just for numerical simulation.

9. In the Hardware tab in Simulink, click the green triangle accompanied by "Monitor & Tune" text. It may take a few seconds and then the experiments starts. If nothing happens, check the [TROUBLESHOTING.md](TROUBLESHOOTING.md).

10. After the experiment starts running, the green triangle should turn into an icon (black square) for stopping the experiment. But if this does not happen (for whatever reason), the fallback solution for stopping the experiment is the "Stop all" option offered through the small Quanser icon at the bottom right panel in Windows. Ultimately there is the RED BUTTON if nothing else works to stop the experiment, this will switch off the amplifier.

11. Note that if you want to record the measured data for later analysis (which you will certainly do), the routes that you are familiar with from purely simulation projects may not work here. The reason is that while using the Quanser QUARC system, you are running the Simulink model in [External Mode](https://www.mathworks.com/help/sldrt/ug/simulink-external-mode.html), which prevents from using some Simulink functionality. In particular, if you [use Simulink scope block(s) for logging the data](https://www.mathworks.com/help/sldrt/ug/set-scope-parameters-for-logging-to-file.html), which is a common route, increasing the parameter called `Limit data points to last` will have no impact on how long the sequence of measurements will be stored in Matlab workspace (but do not forget to tick off the `Log data workspace` option and insert the variable name). If you ignore this issue, you may easily leave the lab assured that you stored a minute or two of experimental data only to discover at home that only the last few seconds have been saved. In order to prevent this, the `Duration` parameter needs to be changed somewhere else. Namely, in the `HARDWARE` tab, click on the `Control Panel` icon. Click on `Signal & Triggering` and change the `Duration` parameter there. If you need more on this, have a look at [the secion on Data Collection in QUARC manual](https://docs.quanser.com/quarc/documentation/quarc_data_collection.html), but if you are happy with logging to the variables in Matlab workspace through the Simulink scope blocks, you should be fine now. 

18. A bit more (but not really much more) can be found in the laboratory guide and data sheet in the [documentation/quanser](documentation/quanser) directory. 