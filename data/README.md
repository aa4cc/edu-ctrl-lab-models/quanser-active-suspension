# Data sets from experiments

## Open-loop experiment

Here we provide the data from the open-loop experiment `software/quanser_updated/q_as_ol.slx`. 
Load the data into Matlab workspace using

```matlab
load measured_and_simulated_open_loop_responses.mat
```

Two new data structs then appear in the workspace
 
- `measured_pos_ol_response`
- `simulated_pos_ol_response`
- `measured_acc_ol_response`

The first two data structures contain (vertical) positions of the three levels/stages: 

- `zr` for the road level,
- `zus` for the suspension level, 
- `zs` for the car level.

Strictly speaking, just `zr` and `zs` are measured by their dedicated sensors. The absolute position `zus` 
is inferred as a difference between `zs` and yet another encoder measurement `zs-zus`.

The third data structs contain just the acceleration of the car level, that is, `d2/dt2 zs`. 

Plot these using

```matlab
figure(1) 
plot(measured_pos_ol_response.time, measured_pos_ol_response.signals.values)
legend('zr','zus','zs')
xlabel('Time [s]')
ylabel('Measured positions [m]')
grid on

figure(2)
plot(simulated_pos_ol_response.time, simulated_pos_ol_response.signals.values)
legend('zr','zus','zs')
xlabel('Time [s]')
ylabel('Simulated positions [m]')
grid on

figure(3)
plot(measured_acc_ol_response.time, measured_acc_ol_response.signals.values)
legend('d2/dt2 zs')
xlabel('Time [s]')
ylabel('Measured acceleration [m/s2]')
grid on
```

You will get something like the figures below

![figures/measured_pos_ol_response.png](figures/measured_pos_ol_response.png)
![figures/simulated_pos_ol_response.png](figures/simulated_pos_ol_response.png)
![figures/measured_acc_ol_response.png](figures/measured_acc_ol_response.png)

Apparently, the simulation responses do not quite fit the experimental ones. Some optimization-based fitting might be needed here.